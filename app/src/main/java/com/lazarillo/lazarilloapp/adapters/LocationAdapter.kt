package com.lazarillo.lazarilloapp.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.lazarillo.lazarilloapp.R
import com.lazarillo.lazarilloapp.models.location

class LocationAdapter(private val locationList : ArrayList<location>) : RecyclerView.Adapter<LocationAdapter.MyViewHolder>() {

    private lateinit var mListener: onItemClickListener

    interface onItemClickListener{

        fun onItemClick(position: Int)
    }

    fun setOnItemClickListener(listener: onItemClickListener){
        mListener = listener
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {

        val itemView = LayoutInflater.from(parent.context).inflate(
            R.layout.location_item,
        parent,false)
        return MyViewHolder(itemView, mListener)

    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {

        val currentItem = locationList[position]

        holder.info.text = currentItem.info
        holder.lat.text = currentItem.lat.toString()
        holder.lon.text = currentItem.lon.toString()

    }

    override fun getItemCount(): Int {
        return locationList.size
    }


    class MyViewHolder(itemView : View, listener: onItemClickListener) : RecyclerView.ViewHolder(itemView){

        val info : TextView = itemView.findViewById(R.id.tvInfo)
        val lat : TextView = itemView.findViewById(R.id.tvLat)
        val lon : TextView = itemView.findViewById(R.id.tvLon)

        init {
            itemView.setOnClickListener{
                listener.onItemClick(adapterPosition)
            }
        }

    }

}