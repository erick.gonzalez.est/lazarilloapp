package com.lazarillo.lazarilloapp.models

class location {
    var info: String? = null
    var lat: Double? = null
    var lon: Double? = null

    constructor() {}

    constructor(info: String?, lat: Double?, lon: Double?) {
        this.info = info
        this.lat = lat
        this.lon = lon
    }
}