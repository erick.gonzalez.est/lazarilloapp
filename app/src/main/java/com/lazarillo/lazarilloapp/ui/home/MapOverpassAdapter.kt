package com.lazarillo.lazarilloapp.ui.home

import android.content.Context
import android.os.Build
import android.os.StrictMode
import android.widget.Button
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.LatLngBounds
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.lazarillo.lazarilloapp.R
import com.lazarillo.library.output.OutputFormat
import com.lazarillo.library.query.OverpassQuery
//import com.lazarillo.library_retrofit_adapter.OverpassQueryResult
//import com.lazarillo.library_retrofit_adapter.OverpassServiceProvider
import hu.supercluster.overpasser.adapter.OverpassQueryResult
import hu.supercluster.overpasser.adapter.OverpassServiceProvider


class MapOverpassAdapter{

    fun search(bounds: LatLngBounds, location: String): OverpassQueryResult? {
        val query: OverpassQuery = OverpassQuery()
            .format(OutputFormat.JSON)
            .timeout(30)
            .filterQuery()
            //                    .node()
            //                    .amenity("parking")
            //                    .tagNot("access", "private")
            .way()
            .tagRegex("name", location)
            .boundingBox(
                bounds.southwest.latitude,
                bounds.southwest.longitude,
                bounds.northeast.latitude,
                bounds.northeast.longitude
            )
            .end()
            .outputB()
            .output(100)
        return interpret(query.build())
    }

    private fun interpret(query: String): OverpassQueryResult? {
        var result: OverpassQueryResult? = null
        return try {
            println(query + "QUERY OVERPASS QL")
            val SDK_INT = Build.VERSION.SDK_INT
            if (SDK_INT > 8) {
                val policy = StrictMode.ThreadPolicy.Builder()
                    .permitAll().build()
                StrictMode.setThreadPolicy(policy)
                result = OverpassServiceProvider.get()!!.interpreter(query)!!.execute().body()
            }
            result
        } catch (e: Exception) {
            e.printStackTrace()
            OverpassQueryResult()
        }
    }

}
