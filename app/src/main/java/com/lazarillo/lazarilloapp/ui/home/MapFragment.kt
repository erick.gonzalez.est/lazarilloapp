package com.lazarillo.lazarilloapp.ui.home

//import com.lazarillo.library_retrofit_adapter.OverpassQueryResult

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.pm.PackageManager
import android.location.Geocoder
import android.location.Location
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationListener
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.*
import com.google.android.gms.maps.model.*
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.lazarillo.lazarilloapp.R
import hu.supercluster.overpasser.adapter.OverpassQueryResult
import org.json.JSONObject
import java.io.IOException
import java.util.*


class MapFragment : Fragment(), OnMapReadyCallback, LocationListener,
    GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    var thiscontext: Context? = null
    lateinit var locationSearch: Spinner
    internal lateinit var mLastLocation: Location
    internal var mCurrLocationMarker: Marker? = null
    internal var mGoogleApiClient: GoogleApiClient? = null
    internal lateinit var mLocationRequest: LocationRequest

    var currentMarker: Marker? = null
    private lateinit var mMap: GoogleMap
    private var currentLocation: Location? = null
    private var fusedLocationProviderClient: FusedLocationProviderClient? = null
    private val REQUEST_CODE = 101

    private lateinit var overApiAdapter: MapOverpassAdapter

    var myRef: DatabaseReference = FirebaseDatabase.getInstance().reference.child("locations")

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        if (container != null) {
            thiscontext = container.context
        }

        return inflater.inflate(R.layout.fragment_map, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val mapFragment = childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment?
        mapFragment?.getMapAsync(this)

        locationSearch = view.findViewById(R.id.editText)

        val dropDownList = arrayOf("Select location", "Buena Vista, USA", "Delhi, India")

        val adapter =
            thiscontext?.let {
                ArrayAdapter(
                    it,
                    android.R.layout.simple_spinner_item,
                    dropDownList
                )
            }
        adapter?.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        locationSearch.adapter = adapter

        val button: Button = view.findViewById(R.id.search_button)
        button.setOnClickListener { view ->

            when {
                thiscontext?.let {
                    ContextCompat.checkSelfPermission(
                        it,
                        Manifest.permission.ACCESS_FINE_LOCATION
                    )
                } == PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(
                    thiscontext!!,
                    Manifest.permission.ACCESS_COARSE_LOCATION
                ) == PackageManager.PERMISSION_GRANTED -> {

                    if (locationSearch.selectedItemPosition == 0) {
                    }

                    if (locationSearch.selectedItemPosition == 1) {
                        condition1()
                    }
                    if (locationSearch.selectedItemPosition == 2) {
                        condition2()
                    }

                }
                shouldShowRequestPermissionRationale("Permission Granted") -> {
                }
                else -> {
                    ActivityCompat.requestPermissions(
                        thiscontext as Activity,
                        arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                        REQUEST_CODE
                    )
                    ActivityCompat.requestPermissions(
                        thiscontext as Activity,
                        arrayOf(Manifest.permission.ACCESS_COARSE_LOCATION),
                        REQUEST_CODE
                    )
                    if (locationSearch.selectedItemPosition == 0) {
                    }

                    if (locationSearch.selectedItemPosition == 1) {
                        condition1()
                    }
                    if (locationSearch.selectedItemPosition == 2) {
                        condition2()
                    }
                }
            }

        }

        val locationButton =
            (view.findViewById<View>(Integer.parseInt("1")).parent as View).findViewById<View>(
                Integer.parseInt(
                    "2"
                )
            )
        val rlp = locationButton.layoutParams as (RelativeLayout.LayoutParams)
        // position on right bottom
        rlp.addRule(RelativeLayout.ALIGN_PARENT_TOP, 0)
        rlp.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE)
        rlp.setMargins(0, 0, 30, 30)

        overApiAdapter = MapOverpassAdapter()

        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(activity)

        fetchLocation()
    }

    private fun condition1() {
        val latLng = LatLng(38.7998805, -106.115967)
        moveMarketBurn(latLng, "Buena Vista, USA")

        myRef.child("names").child("Buena Vista, USA").child("info").setValue(
            getTheAddress(
                38.7998805,
                -106.115967
            )
        )
        myRef.child("names").child("Buena Vista, USA").child("lat").setValue(38.7998805)
        myRef.child("names").child("Buena Vista, USA").child("lon").setValue(-106.115967)
    }

    private fun condition2() {
        val latLng = LatLng(28.4538855, 76.9640974)
        moveMarketBurn(latLng, "Delhi, India")

        myRef.child("names").child("Delhi, India").child("info").setValue(
            getTheAddress(
                28.4538855,
                76.9640974
            )
        )
        myRef.child("names").child("Delhi, India").child("lat").setValue(28.4538855)
        myRef.child("names").child("Delhi, India").child("lon").setValue(76.9640974)
    }

    private fun fetchLocation() {

        if (thiscontext?.let {
                ActivityCompat.checkSelfPermission(
                    it,
                    Manifest.permission.ACCESS_FINE_LOCATION
                )
            } != PackageManager.PERMISSION_GRANTED
            && thiscontext?.let {
                ActivityCompat.checkSelfPermission(
                    it,
                    Manifest.permission.ACCESS_COARSE_LOCATION
                )
            } != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(
                thiscontext as Activity,
                arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                REQUEST_CODE
            )
            return
        }

        val task = fusedLocationProviderClient!!.lastLocation

        task.addOnSuccessListener { location ->
            if (location != null) {
                currentLocation = location
                // Current location on init with error after going to another fragment
                val bundle: Bundle? = activity?.intent?.extras

                if (bundle != null) {
                    val lat = bundle.getDouble("lat")
                    val lon = bundle.getDouble("lon")
                    val newLatLng = LatLng(lat, lon)
                    moveMarket(newLatLng)
                } else {
                    val latLng = LatLng(currentLocation!!.latitude, currentLocation!!.longitude)
                    moveMarket(latLng)
                }

                Toast.makeText(
                    requireActivity().application,
                    currentLocation!!.latitude.toString() + " " + currentLocation!!.longitude,
                    Toast.LENGTH_SHORT
                ).show()
                val supportMapFragment =
                    (childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment?)!!
                supportMapFragment.getMapAsync(this@MapFragment)
            }
        }

    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap

//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//            if (thiscontext?.let {
//                    ContextCompat.checkSelfPermission(
//                        it,
//                        Manifest.permission.ACCESS_FINE_LOCATION
//                    )
//                } == PackageManager.PERMISSION_GRANTED) {
//                buildGoogleApiClient()
//                mMap.isMyLocationEnabled = true
//            }
//        } else {
//            buildGoogleApiClient()
//            mMap.isMyLocationEnabled = true
//        }

        mMap.setOnMarkerDragListener(object : GoogleMap.OnMarkerDragListener {
            override fun onMarkerDragStart(marker: Marker) {}
            override fun onMarkerDragEnd(marker: Marker) {
                Log.d("====", "latitude : " + marker.position.latitude)

                if (currentMarker != null) {
                    currentMarker?.remove()
                }
                val newlatLng = LatLng(marker.position.latitude, marker.position.longitude)
                moveMarket(newlatLng)
            }

            override fun onMarkerDrag(marker: Marker) {}
        })
    }

    private fun moveMarket(latLng: LatLng) {
        val markerOptions = MarkerOptions().position(latLng).title("New location")
            .snippet(getTheAddress(latLng.latitude, latLng.longitude)).draggable(true)
        mMap.animateCamera(CameraUpdateFactory.newLatLng(latLng))
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15f))
        currentMarker = mMap.addMarker(markerOptions)
        currentMarker?.showInfoWindow()
    }

    private fun moveMarketBurn(latLng: LatLng, info: String) {
        val markerOptions = MarkerOptions().position(latLng).title(info)
            .snippet(getTheAddress(latLng.latitude, latLng.longitude)).draggable(true)
        mMap.animateCamera(CameraUpdateFactory.newLatLng(latLng))
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15f))
        currentMarker = mMap.addMarker(markerOptions)
        currentMarker?.showInfoWindow()
    }

    @Synchronized
    protected fun buildGoogleApiClient() {
        mGoogleApiClient = GoogleApiClient.Builder(thiscontext)
            .addConnectionCallbacks(this)
            .addOnConnectionFailedListener(this)
            .addApi(LocationServices.API).build()
        mGoogleApiClient!!.connect()
    }

    override fun onConnected(bundle: Bundle?) {

        mLocationRequest = LocationRequest()
        mLocationRequest.interval = 1000
        mLocationRequest.fastestInterval = 1000
        mLocationRequest.priority = LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY
        if (thiscontext?.let {
                ContextCompat.checkSelfPermission(
                    it,
                    Manifest.permission.ACCESS_FINE_LOCATION
                )
            } == PackageManager.PERMISSION_GRANTED) {
            LocationServices.getFusedLocationProviderClient(activity) // activity instead of context here
        }
    }

    override fun onConnectionSuspended(i: Int) {

    }

    override fun onLocationChanged(location: Location) {

        mLastLocation = location
        if (mCurrLocationMarker != null) {
            mCurrLocationMarker!!.remove()
        }
        //Place current location marker
        val latLng = LatLng(location.latitude, location.longitude)
        val markerOptions = MarkerOptions()
        markerOptions.position(latLng)
        markerOptions.title("Current Position")
        markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN))
        mCurrLocationMarker = mMap.addMarker(markerOptions)

        //move map camera
        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng))
        mMap.animateCamera(CameraUpdateFactory.zoomTo(11f))

        //stop location updates
        if (mGoogleApiClient != null) {
            LocationServices.getFusedLocationProviderClient(activity)
        }

    }

    private fun getTheAddress(latitude: Double, longitude: Double): String? {
        var retVal = ""
        val geocoder = Geocoder(thiscontext, Locale.getDefault())
        try {
            val addresses = geocoder.getFromLocation(latitude, longitude, 1)
            retVal = addresses[0].getAddressLine(0)

        } catch (e: IOException) {
            e.printStackTrace()
        }
        return retVal
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String?>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            REQUEST_CODE -> if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                fetchLocation()
            }
        }
    }

    fun showPoi(poi: OverpassQueryResult.Element) {

//        var location: String = locationSearch.text.toString()
        var location: String = "Av"


        val markerOptions =
            MarkerOptions().position(LatLng(poi.lat, poi.lon)).title(location).snippet(
                getTheAddress(
                    poi.lat,
                    poi.lon
                )
            ).draggable(true)

        if (mCurrLocationMarker != null) {
            mCurrLocationMarker!!.remove()
        }


        mMap.addMarker(markerOptions)

        myRef.child("names").child(location).child("info").setValue(
            getTheAddress(
                poi.lat,
                poi.lon
            )
        )
        myRef.child("names").child(location).child("lat").setValue(poi.lat)
        myRef.child("names").child(location).child("lon").setValue(poi.lon)

        currentMarker = mMap.addMarker(markerOptions)
        currentMarker?.showInfoWindow()

        mMap.animateCamera(CameraUpdateFactory.newLatLng(LatLng(poi.lat, poi.lon)))
        Toast.makeText(
            requireActivity().application,
            poi.lat.toString() + " " + poi.lon,
            Toast.LENGTH_LONG
        ).show()

    }

    fun fetchPois(bounds: LatLngBounds?) {
//        var location: String = locationSearch.text.toString()
        var location: String = "Av"
        val result = overApiAdapter.search(bounds!!, location)
        if (result != null) {
            for (poi in result.elements) {
//      if (!alreadyStored(poi)) {
//        fixTitle(poi)
//        storePoi(poi)
                if (poi.lat != 0.0 && poi.lon != 0.0) {
                    showPoi(poi)
                    break
                }
            }
        }
    }

    override fun onConnectionFailed(connectionResult: ConnectionResult) {
    }

    fun searchPois(view: View) {
        println("working? ")
        fetchPois(getLatLngBounds())
    }

    private fun getLatLngBounds(): LatLngBounds? {
        return mMap.projection.visibleRegion.latLngBounds
    }

    fun getFirstPoi(result: OverpassQueryResult?) {
        val data = StringBuilder()
        val jsonObject = JSONObject(result.toString())
        val jsonArray = jsonObject.optJSONArray("Employee")
        for (i in 0 until jsonArray.length()) {
            val jsonObject = jsonArray.getJSONObject(i)
            val id = jsonObject.optString("ID").toInt()
            val name = jsonObject.optString("Name")
            val salary = jsonObject.optString("Salary").toFloat()
            data.append("Employee ").append(i).append(" : \n ID= ").append(id)
                .append(" \n " + "Name= ").append(
                    name
                ).append(" \n Salary= ").append(salary).append(" \n\n ")
        }
        println("Objecto to string " + data.toString())
    }
}
