package com.lazarillo.lazarilloapp.ui.gallery

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.firebase.database.*
import com.lazarillo.lazarilloapp.MainActivity
import com.lazarillo.lazarilloapp.R
import com.lazarillo.lazarilloapp.adapters.LocationAdapter
import com.lazarillo.lazarilloapp.models.location
import kotlin.collections.ArrayList


class HistoryFragment : Fragment() {

  var thiscontext: Context? = null
  private lateinit var db : DatabaseReference
  private var mLocation: ArrayList<location> = ArrayList()
  var myRef: DatabaseReference = FirebaseDatabase.getInstance().getReference("locations")
  private lateinit var userRecyclerview : RecyclerView

  override fun onCreateView(
    inflater: LayoutInflater,
    container: ViewGroup?,
    savedInstanceState: Bundle?
  ): View? {

    if (container != null) {
      thiscontext = container.context
    };

    return inflater.inflate(R.layout.fragment_history, container, false)
  }

  override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
    super.onViewCreated(view, savedInstanceState)

    db = FirebaseDatabase.getInstance().getReference("locations")

    userRecyclerview = view.findViewById(R.id.locationList)
    userRecyclerview.layoutManager = LinearLayoutManager(thiscontext)
    userRecyclerview.setHasFixedSize(true)

    mLocation = arrayListOf<location>()

    getLocations()

  }

  fun getLocations() {

      db.child("names").addValueEventListener(object : ValueEventListener {
        override fun onDataChange(snapshot: DataSnapshot) {
          if (snapshot.exists()) {

            mLocation.clear()

            for (ds: DataSnapshot in snapshot.children) {
              val info = ds.child("info").value.toString()
              val lat = ds.child("lat").value
              val lon = ds.child("lon").value
              mLocation.add(location(info, lat as Double?, lon as Double?))
            }
//          Toast.makeText(requireActivity().application, "Successfuly Read", Toast.LENGTH_LONG)
//            .show()
            var adapter = LocationAdapter(mLocation)
            userRecyclerview.adapter = adapter
            adapter.setOnItemClickListener(object : LocationAdapter.onItemClickListener {
              override fun onItemClick(position: Int) {
                val intent = Intent(thiscontext, MainActivity::class.java)
                intent.putExtra("info", mLocation[position].info)
                intent.putExtra("lat", mLocation[position].lat)
                intent.putExtra("lon", mLocation[position].lon)
                startActivity(intent)
              }
            })
          }
        }

        override fun onCancelled(error: DatabaseError) {
          Toast.makeText(requireActivity().application, "Error, try again", Toast.LENGTH_LONG)
            .show()
        }

      })
  }
}
