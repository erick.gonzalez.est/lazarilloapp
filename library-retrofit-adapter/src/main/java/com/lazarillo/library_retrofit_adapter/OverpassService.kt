package com.lazarillo.library_retrofit_adapter

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface OverpassService {
    @GET("/api/interpreter")
    fun interpreter(@Query("data") data: String?): Call<OverpassQueryResult?>?
}