package com.lazarillo.library_retrofit_adapter

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object OverpassServiceProvider {
    private var service: OverpassService? = null
    fun get(): OverpassService? {
        if (service == null) {
            service = createService()
        }
        return service
    }

    private fun createService(): OverpassService {
        val retrofit = Retrofit.Builder()
            .baseUrl("http://overpass-api.de")
            .addConverterFactory(GsonConverterFactory.create())
            .build()
        return retrofit.create(OverpassService::class.java)
    }
}