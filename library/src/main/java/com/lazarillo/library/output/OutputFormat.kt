package com.lazarillo.library.output

/**
 * Represents the possible output formats for the query result.
 * Formats "custom" and "popup" are not supported.
 *
 * @see [
 * http://wiki.openstreetmap.org/wiki/Overpass_API/Overpass_QL.Output_Format_.28out.29](http://wiki.openstreetmap.org/wiki/Overpass_API/Overpass_QL.Output_Format_.28out.29)
 */
enum class OutputFormat {
    /**
     * Request the result in JSON format
     */
    JSON,

    /**
     * Request the result in XML format
     */
    XML,

    /**
     * Request the result in CSV format
     */
    CSV
}