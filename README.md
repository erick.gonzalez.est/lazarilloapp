Lazarillo App
===================================
This is a demo requested by the Lazarillo Company

Pre-requisites
--------------

- Android SDK v30
- Min SKD 16
- Latest Android Build Tools
- Android Support Repository
- Google Repository
- Google Play Services
- Firebase Account

- Google Play Services
For google play services you will need a Google Maps API key.

To get one, follow this link, follow the directions and press "Create" at the end:

https://console.developers.google.com/flows/enableapi?apiid=maps_android_backend&keyType=CLIENT_SIDE_ANDROID&r=BE:97:F0:80:7E:7B:8D:73:EB:60:50:37:5A:60:43:24:5E:95:B5:F7%3Bcom.lazarillo.lazarilloapp

You can also add your credentials to an existing key, using these values:

Package name:
com.lazarillo.lazarilloapp

SHA-1 certificate fingerprint:
BE:97:F0:80:7E:7B:8D:73:EB:60:50:37:5A:60:43:24:5E:95:B5:F7

if you run the applicationen and you have problems with the Google Maps API key, the problem may be the SHA-1 certificate in that case you will have to create your own Google Maps API key with the SHA-1 certificate that Android Studio gives you when the API key shows you the error

Alternatively, follow the directions here:
https://developers.google.com/maps/documentation/android/start#get-key

Once you have your key (it starts with "AIza"), replace the "google_maps_key" string
in the following directory "res\values\google_maps_key.xml"

- Firebase Setup
Finally, this application works with a real-time database provided with firebase so that to work locally you must configure your account with Android Studio, this can be done in "Tools \ Firebase \ Realtime Database \ Get started with Realtime Database "

See more here:
https://firebase.google.com/docs/database
